;;; lll-cmd-config.el --- lll cmd config

;; Copyright (C) 2020 luislain.com

;; Author: Luis L. Lazaro <lll@luislain.com>
;; Maintainer: lll@luislain.com
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; This package allows you to manage tasks from Emacs.

;;; Code:

;;;###autoload
(defun lll-cmd-config ()
  "."
  (interactive)
  )

(provide 'lll-cmd-config)
;;; lll-cmd-config.el ends here
