;;; lll-cmd-aws-s3.el --- lll cmd action

;; Copyright (C) 2020 luislain.com

;; Author: Luis L. Lazaro <lll@luislain.com>
;; Maintainer: lll@luislain.com
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; This package allows you to manage tasks from Emacs.

;;; Code:

(require 'lll-cmd-action)

(defvar lll-cmd-aws-s3-command
  " s3 ls"
  ".")

(defvar lll-cmd-aws-s3-cols
  [("DATE" 15 t) ("TIME" 15 t) ("BUCKET" 35 t)]
  ".")

(defun lll-cmd-aws-s3-row-parse (line)
  "LINE."
  (let ((line-fields (split-string line)))
    (list (car (last line-fields)) (vconcat line-fields))))

(defun lll-cmd-aws-s3-rows ()
  "."
  (let* ((data (shell-command-to-string (concat lll-cmd-aws-command
						lll-cmd-aws-arguments
						lll-cmd-aws-s3-command)))
	 (lines (split-string data "[\n\r]" t)))
    (message data)
    (mapcar 'lll-cmd-aws-s3-row-parse lines)))

;;;###autoload
(defun lll-cmd-aws-s3 ()
  "."
  (interactive)
  (lll-cmd-action "lll-cmd-aws-s3"))

(provide 'lll-cmd-aws-s3)
;;; lll-cmd-aws-s3.el ends here
