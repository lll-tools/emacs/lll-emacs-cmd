;;; lll-cmd-action.el --- lll cmd action

;; Copyright (C) 2020 luislain.com

;; Author: Luis L. Lazaro <lll@luislain.com>
;; Maintainer: lll@luislain.com
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; This package allows you to manage tasks from Emacs.

;;; Code:

(require 'tablist)

(defvar lll-cmd-action-cols)
;; (defun lll-cmd-action-rows ())

(defun lll-cmd-action-refresh ()
  "Refresh CMD entries."
  (setq tabulated-list-entries (lll-cmd-action-rows)))

(define-derived-mode lll-cmd-action-mode tabulated-list-mode
  "LLL CMD mode"
  (setq tabulated-list-format lll-cmd-action-cols)
  (setq tabulated-list-padding 2)
  (add-hook 'tabulated-list-revert-hook 'lll-cmd-action-refresh nil t)
  (tabulated-list-init-header)
  (tablist-minor-mode))

;;;###autoload
(defun lll-cmd-action (lll-cmd-action-name)
  "LLL CMD action.  LLL-CMD-ACTION-NAME."
  (interactive)
  (let (
	(cols-name (concat lll-cmd-action-name "-cols"))
	(rows-name (concat lll-cmd-action-name "-rows"))
	)
    (setq lll-cmd-action-cols (symbol-value (intern cols-name)))
    (fset 'lll-cmd-action-rows (symbol-function (intern rows-name)))
    )
  (pop-to-buffer (concat "*" lll-cmd-action-name "*"))
  (lll-cmd-action-mode)
  (tabulated-list-revert))

(provide 'lll-cmd-action)
;;; lll-cmd-action.el ends here
