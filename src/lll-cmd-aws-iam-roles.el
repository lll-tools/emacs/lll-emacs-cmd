;;; lll-cmd-aws-iam-roles.el --- lll cmd action AWS IAM Roles

;; Copyright (C) 2020 luislain.com

;; Author: Luis L. Lazaro <lll@luislain.com>
;; Maintainer: lll@luislain.com
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; This package allows you to manage tasks from Emacs.

;;; Code:

(require 'lll-cmd-action)

(defvar lll-cmd-aws-iam-roles-command
  " iam list-roles"
  ".")

(defvar lll-cmd-aws-iam-roles-cols
  [
   ("CreateDate" 25 t)
   ("RoleId" 21 t)
   ("RoleName" 40 t)
   ("Path" 30 t)
   ("Arn" 40 t)
   ;; ("MaxSessionDuration" 35 t)
   ]
  "Columns definition.")

(defun lll-cmd-aws-iam-roles-row-parse (row)
  "Convert ROW to a `tabulated-list-entries' entry."
  (condition-case nil
      (let* (
	     (lll-iam-roles-row (vconcat row))
      	     (lll-iam-roles-row-length (length lll-iam-roles-row))
	     (lll-iam-roles-row-name (alist-get 'RoleName row))
	     (lll-iam-roles-row-path (alist-get 'Path row))
	     (lll-iam-roles-row-id (alist-get 'RoleId row))
	     (lll-iam-roles-row-arn (alist-get 'Arn row))
	     (lll-iam-roles-row-date (alist-get 'CreateDate row))
	     ;; (lll-iam-roles-row-max (alist-get 'MaxSessionDuration row))
	     ;; (lll-iam-roles-row-policy (alist-get 'AssumeRolePolicyDocument row))
	     )
	;; (aset lll-iam-roles-row 2
	;;       (propertize lll-iam-roles-status
	;; 		  'font-lock-face (lll-aws-iam-roles-status-face lll-iam-roles-status)))
      	(list lll-iam-roles-row-name (vector lll-iam-roles-row-date
					       lll-iam-roles-row-id
					       lll-iam-roles-row-name
					       lll-iam-roles-row-path
					       lll-iam-roles-row-arn
					       ;; lll-iam-roles-row-max
					       )))
    (error "Could not read string:\n%s" row))
  )

(defun lll-cmd-aws-iam-roles-rows ()
  "Rows definition."
  (let* ((data (shell-command-to-string (concat lll-cmd-aws-command
						lll-cmd-aws-arguments
						lll-cmd-aws-iam-roles-command)))
	 (datajson (condition-case nil
		       (json-read-from-string data)
		     (error nil)))
	 (rows (alist-get 'Roles datajson))
	 )
    (mapcar 'lll-cmd-aws-iam-roles-row-parse rows))
  )

;;;###autoload
(defun lll-cmd-aws-iam-roles ()
  "."
  (interactive)
  (lll-cmd-action "lll-cmd-aws-iam-roles"))

(provide 'lll-cmd-aws-iam-roles)
;;; lll-cmd-aws-iam-roles.el ends here
