;;; lll-cmd-aws-config.el --- lll cmd aws-config

;; Copyright (C) 2020 luislain.com

;; Author: Luis L. Lazaro <lll@luislain.com>
;; Maintainer: lll@luislain.com
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; This package allows you to manage tasks from Emacs.

;;; Code:

;;;###autoload
(defun lll-cmd-aws-config ()
  "."
  (interactive)
  )

(provide 'lll-cmd-aws-config)
;;; lll-cmd-aws-config.el ends here
