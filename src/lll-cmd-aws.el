;;; lll-cmd-aws.el --- Emacs interface for CLIs

;; Copyright (C) 2020 luislain.com

;; Author: Luis L. Lazaro <lll@luislain.com>
;; Maintainer: lll@luislain.com
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; This package allows you to manage tasks from Emacs.

;;; Code:

(require 'lll-cmd-aws-config)
(require 'lll-cmd-aws-s3)
(require 'lll-cmd-aws-iam-roles)
(require 'lll-cmd-aws-lambda-functions)

(defgroup lll-cmd-aws nil
  "LLL aws customization group."
  :group 'convenience)

(defcustom lll-cmd-aws-command "aws"
  "LLL aws command."
  :group 'lll-cmd
  :type 'string)

(defcustom lll-cmd-aws-arguments ""
  "Arguments for aws command."
  :group 'lll-cmd
  :type 'string)

(defun lll-cmd-aws-heading ()
  "Show heading info."
  (format "%s"
	  (propertize "LLL CMD AWS" 'face 'transient-heading)
	  )
  )

;;;###autoload (autoload 'lll-cmd-aws "lll-cmd-aws" nil t)
(transient-define-prefix lll-cmd-aws ()
  "Transient for lll-cmd-aws."
  :man-page "lll-cmd-aws"
  ["Arguments"
   ("-F"	"File"		"--file "	read-string)
   ]
  [:description lll-cmd-aws-heading
   ("s" "S3"			lll-cmd-aws-s3)
   ("r" "IAM Roles"    		lll-cmd-aws-iam-roles)
   ("f" "Lambda Functions"    	lll-cmd-aws-lambda-functions)
   ]
  ["Other"
   ("C" "AWS Config"	lll-cmd-aws-config)
   ])

(provide 'lll-cmd-aws)
;;; lll-cmd-aws.el ends here
