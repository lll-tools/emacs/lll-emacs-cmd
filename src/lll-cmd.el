;;; lll-cmd.el --- Emacs interface for CLIs

;; Copyright (C) 2020 luislain.com

;; Author: Luis L. Lazaro <lll@luislain.com>
;; Maintainer: lll@luislain.com
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; This package allows you to manage tasks from Emacs.

;;; Code:

(require 'lll-cmd-aws)
(require 'lll-cmd-config)

(defun lll-cmd-heading ()
  "Show heading info."
  (format "%s"
	  (propertize "LLL CMD" 'face 'transient-heading)
	  )
  )

;;;###autoload (autoload 'lll-cmd "lll-cmd" nil t)
(define-transient-command lll-cmd ()
  "Transient for lll-cmd."
  :man-page "lll-cmd"
  ["Arguments"
   ("-F"	"File"		"--file "	read-string)
   ]
  [:description lll-cmd-heading
   ("w" "AWS"		lll-cmd-aws)
   ]
  ["Other"
   ("C" "Config"	lll-cmd-config)
   ])

(provide 'lll-cmd)
;;; lll-cmd.el ends here
