;;; lll-cmd-aws-lambda-functions.el --- lll cmd action AWS IAM Roles

;; Copyright (C) 2020 luislain.com

;; Author: Luis L. Lazaro <lll@luislain.com>
;; Maintainer: lll@luislain.com
;; Version: 0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:

;; This package allows you to manage tasks from Emacs.

;;; Code:

(require 'lll-cmd-action)

(defvar lll-cmd-aws-lambda-functions-command
  " lambda list-functions"
  ".")

(defvar lll-cmd-aws-lambda-functions-cols
  [
   ("LastModified" 30 t)
   ("RevisionId" 40 t)
   ("FunctionName" 25 t)
   ("Handler" 20 t)
   ("Runtime" 15 t)
   ;; ("MemorySize" 30 t)
   ("Pck-Type" 9 t)
   ("FunctionArn" 45 t)
   ]
  "Columns definition.")

(defun lll-cmd-aws-lambda-functions-row-parse (row)
  "Convert ROW to a `tabulated-list-entries' entry."
  (condition-case nil
      (let* (
	     (lll-lambda-functions-row (vconcat row))
      	     (lll-lambda-functions-row-length (length lll-lambda-functions-row))
	     (lll-lambda-functions-row-name (alist-get 'FunctionName row))
	     (lll-lambda-functions-row-arn (alist-get 'FunctionArn row))
	     (lll-lambda-functions-row-runtime (alist-get 'Runtime row))
	     (lll-lambda-functions-row-handler (alist-get 'Handler row))
	     ;; (lll-lambda-functions-row-mem (alist-get 'MemorySize row))
	     (lll-lambda-functions-row-date (alist-get 'LastModified row))
	     (lll-lambda-functions-row-id (alist-get 'RevisionId row))
	     (lll-lambda-functions-row-type (alist-get 'PackageType row))
	     )
      	(list lll-lambda-functions-row-name (vector lll-lambda-functions-row-date
					       lll-lambda-functions-row-id
					       lll-lambda-functions-row-name
					       lll-lambda-functions-row-handler
					       lll-lambda-functions-row-runtime
					       ;; lll-lambda-functions-row-mem
					       lll-lambda-functions-row-type
					       lll-lambda-functions-row-arn
					       )))
    (error "Could not read string:\n%s" row))
  )

(defun lll-cmd-aws-lambda-functions-rows ()
  "Rows definition."
  (let* ((data (shell-command-to-string (concat lll-cmd-aws-command
						lll-cmd-aws-arguments
						lll-cmd-aws-lambda-functions-command)))
	 (datajson (condition-case nil
		       (json-read-from-string data)
		     (error nil)))
	 (rows (alist-get 'Functions datajson))
	 )
    (mapcar 'lll-cmd-aws-lambda-functions-row-parse rows))
  )

;;;###autoload
(defun lll-cmd-aws-lambda-functions ()
  "."
  (interactive)
  (lll-cmd-action "lll-cmd-aws-lambda-functions"))

(provide 'lll-cmd-aws-lambda-functions)
;;; lll-cmd-aws-lambda-functions.el ends here
